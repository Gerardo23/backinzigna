from os import path
from bs4 import BeautifulSoup
from dotenv import load_dotenv
from datetime import datetime, date
import re, ftplib, random, string, base64, time, os, pytz
load_dotenv()


def find_item_by_filter(client, app_id, filters):
    resp = client.Item.filter(
        app_id=app_id, attributes={
            'limit': 500,
            'filters': filters})

    return resp


def create_item(client, app_id, fields):
    resp = client.Item.create(
        app_id=app_id, attributes={
            "fields": fields})

    return resp


def update_item(client, item_id, fields):
    resp = client.Item.update(
        item_id, attributes={
            "fields": fields})

    return resp


def delete_item(client, item_id):
    resp = client.Item.delete(item_id)

    return resp


def find_item_id(client, item_id):
    resp = client.Item.find(
        item_id
    )

    return resp


def get_all_item_fields(fields_list, item_id=None, broker=False, client=False):
    podio_fields = {}
    index = 0
    relations = []

    for field in fields_list:
        if field["type"] == "email":
            podio_fields.update(
                {
                    field["external_id"]: field["values"]
                }
            )
        elif field["type"] == "category":
            podio_fields.update(
                {
                    field["external_id"]: field["values"][0]['value']['text']
                }
            )
        elif field["type"] == "date":
            podio_fields.update(
                {
                    field["external_id"]: field["values"][0]["start"]
                }
            )
        elif field["type"] == "app":
            podio_fields.update(
                {
                    field["external_id"]: {
                        "item_id": field["values"][0]["value"]["item_id"],
                        "title": field["values"][0]["value"]["title"]
                    }
                }
            )
        elif field["type"] == "money":
            podio_fields.update(
                {
                    field["external_id"]: field["values"]
                }
            )
        else:
            if 'value' in field['values'][0]:
                podio_fields.update(
                    {
                        field["external_id"]: field["values"][0]["value"]
                    }
                )
            else:
                podio_fields.update(
                    {
                        field["external_id"]: field["values"]
                    }
                )

    if item_id:
        podio_fields.update(
            {'item_id': item_id}
        )
    return podio_fields

def find_item_by_filter_from_app(client, app_id, filter_dict, inspect=False):
    resp = client.Item.filter(
        app_id=app_id,
        attributes={
            'limit': 500,
            'filters': filter_dict
        }
    )

    items_list = resp['items']
    all_items_fields_list = []

    for item in items_list:
        item['fields'].append({
            'external_id': 'item_id',
            'type': 'id',
            'values': [
                {'value': item['item_id']}
            ]
        })
        all_items_fields_list.append(item['fields'])

    items_list.clear()
    # return all_items_fields_list
    for fields_list in all_items_fields_list:
        podio_fields = {}

        for field in fields_list:
            if 'value' in field['values'][0]:
                if field["type"] == "email":
                    podio_fields.update(
                        {field["external_id"]: field["values"][0]['value']})
                elif field["type"] == "category":
                    podio_fields.update(
                        {field["external_id"]: field["values"][0]['value']['text']})
                elif field["type"] == "app":
                    podio_fields.update(
                        {
                    field["external_id"]: {
                        "item_id": field["values"][0]["value"]["item_id"],
                        "title": field["values"][0]["value"]["title"]
                    }
                })
                elif field["type"] == "money":
                    podio_fields.update(
                        {"final-currency": field["values"][0]["currency"]})
                    podio_fields.update(
                        {field["external_id"]: field["values"][0]["value"]})
                elif field["type"] == "number":
                    podio_fields.update(
                        {field["external_id"]: str(int(float(field["values"][0]["value"])))})
                elif field["type"] == "image":
                    field["type"] = field["type"]
                else:
                    if(field["external_id"] == "url-image"):
                        podio_fields.update(
                            {field["external_id"]: field["values"][0]["value"].split(",")})
                    else:
                        podio_fields.update(
                            {field["external_id"]: field["values"][0]["value"]})
            else:
                if field["type"] == "date":
                    podio_fields.update(
                        {field["external_id"]: field["values"][0]["start"]})
                else:
                    podio_fields.update(
                        {field["external_id"]: field["values"]})

        items_list.append(podio_fields)

    return items_list


def upload_file(server,user,password,file, folder):

    working_dict = os.getcwd()

    document_file = file
    file_name = rename(document_file.filename)
    file_path = working_dict + '/' + file_name
    document_file.save(file_path)
    
    ftp = ftplib.FTP(server)
    ftp.login(user, password)
    
    try:
        ftp.cwd(folder)
    except:
        ftp.mkd(folder)
        ftp.retrbinary("RETR index.html",open("index.html","wb").write)
        ftp.cwd(folder)
        f = open("index.html", 'rb')
        ftp.storbinary('STOR index.html', f)

    f = open(file_path, 'rb')
    ftp.storbinary('STOR '+str(file_name), f)
    f.close()
    ftp.quit()
    
    url_voucher = 'https://archivos.dsindigo.com/' + folder +"/" + file_name

    os.remove(file_path)
    if(os.path.isfile('index.html')):
        os.remove('index.html')
    
    return {
        "url": url_voucher,
        "name": file_name
    }

def today():
    UTC = pytz.utc
    IST = pytz.timezone('America/Mexico_City')
    datetime_ist = datetime.now(IST)
    #2021:08:26 17:01:30 CDT -0500
    fecha = datetime_ist.strftime('%Y:%m:%d %H:%M:%S %Z %z')

    return fecha[0:11].replace(":","-") + fecha[11:19]

def rename(name_file):
    name_file = name_file.replace("ñ","n")
    name_file = name_file.replace("Ñ","N")
    name_file = name_file.replace("Á","A")
    name_file = name_file.replace("É","E")
    name_file = name_file.replace("Í","I")
    name_file = name_file.replace("Ó","O")
    name_file = name_file.replace("Ú","U")
    name_file = name_file.replace("-","_")
    name_file = name_file.replace("&","y")
    name_file = name_file.replace("(","_")
    name_file = name_file.replace(")","_")
    name_file = name_file.replace("*","_")
    name_file = name_file.replace("+","_")
    name_file = name_file.replace("/","_")
    name_file = name_file.replace("\\","_")
    name_file = name_file.replace("¿","_")
    name_file = name_file.replace("?","_")
    name_file = name_file.replace("<","_")
    name_file = name_file.replace(">","_")
    name_file = name_file.replace(":","_")
    name_file = name_file.replace(" ","_")
    name_file = name_file.replace("á","a")
    name_file = name_file.replace("é","e")
    name_file = name_file.replace("í","i")
    name_file = name_file.replace("ó","o")
    name_file = name_file.replace("ú","u")
    regex = re.compile('^[a-zA-Z_0-9]')
    dinamico = ''.join(random.choices(string.ascii_uppercase + string.digits, k=8))
    return regex.sub('', dinamico+name_file)


def orderByDate(list_date):
    
    estatus = True
    aux_lista_date = []
    while(estatus):
        aux = list_date[0]
        for fecha in list_date:

            date1 = time.strptime(aux["fecha"].replace("-","/"), "%Y/%m/%d %H:%M:%S")
            date2 = time.strptime(fecha["fecha"].replace("-","/"), "%Y/%m/%d %H:%M:%S")

            if(date1>date2):
                aux = fecha
                
        aux_lista_date.append(aux)
        list_date.remove(aux)
        
        if(len(list_date)==0):
            estatus=False

    return aux_lista_date