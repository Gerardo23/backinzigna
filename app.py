from flask import Flask, request, jsonify
from dotenv import load_dotenv
from flask_jwt_extended import (
    JWTManager,
)
from flask_cors import CORS

##### IMPORTING BLUEPRINTS #####
# Service Login
from services.generalServices import general
# General Services
#from services.solicitudes import solicitudes
#from services.conversaciones import conversacion
from services.usuarios import usuarios
from services.departamentos import departamentos
from services.desarrollos import desarrollos
from services.upload_file import uploadImage
from services.cotizaciones import cotizaciones
from services.clientes import clientes
from services.pdfs import pdfs


###################################################

app = Flask(__name__)
app.register_blueprint(general)
app.register_blueprint(usuarios)
app.register_blueprint(uploadImage)
app.register_blueprint(departamentos)
app.register_blueprint(desarrollos)
app.register_blueprint(cotizaciones)
app.register_blueprint(clientes)
app.register_blueprint(pdfs)


app.config['SECRET_KEY'] = 'TGjrYOh7glAs3jG1GOiuJ5K4jeMjmlndOJtDyCGg4nYhemmy58xatf5icLD7'
jwt = JWTManager(app)

CORS(app)

if __name__ == "__main__":
    app.run(debug=True, port=8443, host="0.0.0.0")
