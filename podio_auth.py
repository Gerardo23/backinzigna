import os
from pypodio2 import api
from dotenv import load_dotenv
load_dotenv()
# Authenticate as App in podio, variables below contains a client object that
# allow us to do all the diferent requests to Podio API


# Client to do requests to Usuarios in app Inzigna
def podio_client_users():
    return api.OAuthAppClient(
        client_id=os.getenv("PODIO_CLIENT_ID"),
        client_secret=os.getenv("PODIO_CLIENT_SECRET"),
        app_id=os.getenv("PODIO_INZIGNA_USERS_APP_ID"),
        app_token=os.getenv("PODIO_INZIGNA_USERS_APP_TOKEN"),
    )


# Client to do requests to Departamentos in app Inzigna
def podio_client_departamentos():
    return api.OAuthAppClient(
        client_id=os.getenv("PODIO_CLIENT_ID"),
        client_secret=os.getenv("PODIO_CLIENT_SECRET"),
        app_id=os.getenv("PODIO_INZIGNA_DEPARTAMENTOS_APP_ID"),
        app_token=os.getenv("PODIO_INZIGNA_DEPARTAMENTOS_APP_TOKEN"),
    )


# Client to do requests to Desarrollos in app Inzigna
def podio_client_desarrollos():
    return api.OAuthAppClient(
        client_id=os.getenv("PODIO_CLIENT_ID"),
        client_secret=os.getenv("PODIO_CLIENT_SECRET"),
        app_id=os.getenv("PODIO_INZIGNA_DESARROLLOS_APP_ID"),
        app_token=os.getenv("PODIO_INZIGNA_DESARROLLOS_APP_TOKEN"),
    )
    
# Client to do requests to Cotizaciones in app Inzigna
def podio_client_cotizaciones():
    return api.OAuthAppClient(
        client_id=os.getenv("PODIO_CLIENT_ID"),
        client_secret=os.getenv("PODIO_CLIENT_SECRET"),
        app_id=os.getenv("PODIO_INZIGNA_COTIZACIONES_APP_ID"),
        app_token=os.getenv("PODIO_INZIGNA_COTIZACIONES_APP_TOKEN"),
    )

# Client to do requests to Historial in app Inzigna
def podio_client_historial():
    return api.OAuthAppClient(
        client_id=os.getenv("PODIO_CLIENT_ID"),
        client_secret=os.getenv("PODIO_CLIENT_SECRET"),
        app_id=os.getenv("PODIO_INZIGNA_HISTORIAL_APP_ID"),
        app_token=os.getenv("PODIO_INZIGNA_HISTORIAL_APP_TOKEN"),
    )

# Client to do requests to Historial in app Inzigna
def podio_client_imagenes():
    return api.OAuthAppClient(
        client_id=os.getenv("PODIO_CLIENT_ID"),
        client_secret=os.getenv("PODIO_CLIENT_SECRET"),
        app_id=os.getenv("PODIO_INZIGNA_IMAGENES_APP_ID"),
        app_token=os.getenv("PODIO_INZIGNA_IMAGENES_APP_TOKEN"),
    )

# Client to do requests to Configuracion in app Inzigna
def podio_client_configuracion():
    return api.OAuthAppClient(
        client_id=os.getenv("PODIO_CLIENT_ID"),
        client_secret=os.getenv("PODIO_CLIENT_SECRET"),
        app_id=os.getenv("PODIO_INZIGNA_CONFIGURACION_APP_ID"),
        app_token=os.getenv("PODIO_INZIGNA_CONFIGURACION_APP_TOKEN"),
    )

def podio_user_simon():
    return api.OAuthClient(
        api_key=os.getenv("PODIO_USER_API_KEY"),
        api_secret=os.getenv("PODIO_USER_API_SECRET"), 
        login=os.getenv("PODIO_USER_LOGIN_SIMON"), 
        password=os.getenv("PODIO_USER_PASSWORD_SIMON"))