##### IMPORTING MODULES #####
from user_manager import *
from podio_auth import (
    podio_client_departamentos
)
from flask import (
    jsonify,
    request,
    Blueprint
)
from flask_jwt_extended import (
    jwt_required,
    get_jwt_identity
)
import os, ftplib, subprocess
from dotenv import load_dotenv
load_dotenv()
######################################

departamentos = Blueprint('departamentos', __name__)

@departamentos.route('/get-departments', methods=['GET'])
@jwt_required
def get_departments():
    ###### Get current user ################
    current_user = get_jwt_identity()

    departamentos = find_item_by_filter_from_app(
        podio_client_departamentos(),
        int(os.getenv("PODIO_INZIGNA_DEPARTAMENTOS_APP_ID")),
        {
        }
    )

    return jsonify(departamentos)

@departamentos.route('/get-departments-for-development', methods=['POST'])
@jwt_required
def get_departments_for_development():
    ###### Get current user ################
    current_user = get_jwt_identity()
    
    req_data = request.get_json()

    departamentos = find_item_by_filter_from_app(
        podio_client_departamentos(),
        int(os.getenv("PODIO_INZIGNA_DEPARTAMENTOS_APP_ID")),
        {
            "desarrollo-2": int(req_data["desarrollo"]),
        }
    )

    return jsonify(departamentos)

@departamentos.route('/create-department', methods=['POST'])
@jwt_required
def create_department():
    ###### Get current user ################
    current_user = get_jwt_identity()
    
    req_data = request.get_json()

    url_image = ""

    #1 aprobada
    #2 cancelada
    #3 enviada
    #4 rechazada
    #5 revision
    #6 disponible
    #7 vendido
    #8 reservado
    #9 bloqueado

    if(req_data["estatus"] == 'Disponible'): 
        estatus = 6
    if(req_data["estatus"] == 'Vendido'): 
        estatus = 7
    if(req_data["estatus"] == 'Reservado'): 
        estatus = 8
    if(req_data["estatus"] == 'Bloqueado'): 
        estatus = 9
    
    if req_data["imagenes"]!='':
        for cont in range(0,len(req_data["imagenes"])):
            if(cont == (len(req_data["imagenes"]))-1):
                url_image = url_image + req_data["imagenes"][cont]
            else:
                url_image = url_image + req_data["imagenes"][cont] + ","

    create_item(
        podio_client_departamentos(),
        int(os.getenv("PODIO_INZIGNA_DEPARTAMENTOS_APP_ID")),
        {
            "desarrollo-2": int(req_data["desarrollo"]),
            "estatus": estatus,
            "precio-lista": int(req_data["precioLista"]),
            "piso": req_data["piso"],
            "torre": req_data["torre"],
            "departamento": req_data["departamento"],
            "tipologia": req_data["tipologia"],
            "descripcion": req_data["descripcion"],
            "area-interior": req_data["areaInterior"],
            "terraza": req_data["terrasa"],
            "roof-garden": req_data["roofGarden"],
            "total-m2": req_data["totalM2"],
            "total-sqrt": req_data["totalSqrt"],
            "imagen-posicionamiento": req_data["posicionFile"],
            "imagen-plano-isometrico": req_data["planoIsometricoFile"],
            "texto":url_image,
        },
    )

    return {
        "msg": "Agregado correctamente",
    }

@departamentos.route('/update-department', methods=['POST'])
@jwt_required
def update_department():

      ###### Get current user ################
    current_user = get_jwt_identity()
    
    req_data = request.get_json()

    filters = {
        "item_id": int(req_data["id"])
    }

    departamento = find_item_by_filter_from_app(
        podio_client_departamentos(),
        int(os.getenv("PODIO_INZIGNA_DEPARTAMENTOS_APP_ID")),
        filters,
    )
    # print(req_data)
    # print(departamento)
    
    url_image = ""
    posicion = ""
    isometrico = ""
    
    if(req_data["estatus"] == 'Disponible'): 
        estatus = 6
    if(req_data["estatus"] == 'Vendido'): 
        estatus = 7
    if(req_data["estatus"] == 'Reservado'): 
        estatus = 8
    if(req_data["estatus"] == 'Bloqueado'): 
        estatus = 9

    if req_data["posicionFile"]==[]:
        posicion = departamento[0]["imagen-posicionamiento"]
    else:
        posicion = req_data["posicionFile"]
        
    if req_data["planoIsometricoFile"]==[None]:
        isometrico = departamento[0]["imagen-plano-isometrico"]
    else:
        isometrico = req_data["planoIsometricoFile"]

    if req_data["imagenes"]!=[None]:
        for cont in range(0,len(req_data["imagenes"])):
            if(cont == (len(req_data["imagenes"]))-1):
                url_image = url_image + req_data["imagenes"][cont]
            else:
                url_image = url_image + req_data["imagenes"][cont] + ","
    else:
        url_image = departamento[0]["texto"]

    departamento = update_item(
        podio_client_departamentos(),
        int(req_data["id"]),
        {
           "desarrollo-2": int(req_data["desarrollo"]),
            "estatus": estatus,
            "precio-lista": int(req_data["precioLista"]),
            "piso": req_data["piso"],
            "torre": req_data["torre"],
            "departamento": req_data["departamento"],
            "tipologia": req_data["tipologia"],
            "descripcion": req_data["descripcion"],
            "area-interior": req_data["areaInterior"],
            "terraza": req_data["terrasa"],
            "roof-garden": req_data["roofGarden"],
            "total-m2": req_data["totalM2"],
            "total-sqrt": req_data["totalSqrt"],
            "imagen-posicionamiento":posicion,
            "imagen-plano-isometrico": isometrico,
            "texto":url_image,
        }
    )

    return { "msg": "Actualizado Correctamente"}

    

@departamentos.route('/delete-department', methods=['POST'])
@jwt_required
def delete_department():
     ###### Get current user ################
    current_user = get_jwt_identity()

    req_data = request.get_json()

    departamento = delete_item(
        podio_client_departamentos(),
        int(req_data["id"])
    )
    
    return { "msg": "Proceso Correcto"}

@departamentos.route('/get-department-by-id', methods=['POST'])
@jwt_required
def get_department_by_id():
    ###### Get current user ################
    current_user = get_jwt_identity()
    req_data = request.get_json()
    
    filters = {
        "item_id": int(req_data["id"])
    }

            
    departamento = find_item_by_filter_from_app(
        podio_client_departamentos(),
        int(os.getenv("PODIO_INZIGNA_DEPARTAMENTOS_APP_ID")),
        filters,
    )

    return jsonify(departamento)