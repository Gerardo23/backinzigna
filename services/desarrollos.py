##### IMPORTING MODULES #####
from user_manager import *
from podio_auth import (
    podio_client_desarrollos
)
from flask import (
    jsonify,
    request,
    Blueprint
)
from flask_jwt_extended import (
    jwt_required,
    get_jwt_identity
)
import os, ftplib, subprocess
from dotenv import load_dotenv
import requests, base64
import img2pdf 
from PIL import Image
load_dotenv()
######################################

desarrollos = Blueprint('desarrollos', __name__)

@desarrollos.route('/get-developments', methods=['GET'])
@jwt_required
def get_developments():
    ###### Get current user ################
    current_user = get_jwt_identity()

    usuarios = find_item_by_filter_from_app(
        podio_client_desarrollos(),
        int(os.getenv("PODIO_INZIGNA_DESARROLLOS_APP_ID")),
        {
        }
    )

    return jsonify(usuarios)

@desarrollos.route('/create-development', methods=['POST'])
@jwt_required
def create_development():
    ###### Get current user ################
    current_user = get_jwt_identity()
    
    req_data = request.get_json()
    
    url_image = ""
    
    if req_data["logo"]=='':
        req_data["logo"] = 'Sin información'
    if req_data["portada"]=='':
        req_data["portada"] = 'Sin información'
    if req_data["posicionFile"]=='':
        req_data["posicionFile"] = 'Sin información'
    if req_data["amenidadesFile"]=='':
        req_data["amenidadesFile"] = 'Sin información'
    if req_data["cotizacionFile"]=='':
        req_data["cotizacionFile"] = 'Sin información'

    if req_data["imagenes"]!='':
        for cont in range(0,len(req_data["imagenes"])):
            if(cont == (len(req_data["imagenes"]))-1):
                url_image = url_image + req_data["imagenes"][cont]
            else:
                url_image = url_image + req_data["imagenes"][cont] + ","

    create_item(
        podio_client_desarrollos(),
        int(os.getenv("PODIO_INZIGNA_DESARROLLOS_APP_ID")),
        {
            "imagen-logo": req_data["logo"],
            "nombre": req_data["nombre"],
            "ciudad": req_data["ciudad"],
            "direccion": req_data["direccion"],
            "link": req_data["link"],
            "imagen-portada": req_data["portada"],
            "imagen-mapa": req_data["posicionFile"],
            "imagen-amenidades": req_data["amenidadesFile"],
            "imagen-cotizacion":  req_data["cotizacionFile"],
            "imagenes-3": url_image,
        },
    )

    return {
        "msg": "Agregado correctamente",
    }

@desarrollos.route('/update-development', methods=['POST'])
@jwt_required
def update_development():
    ###### Get current user ################
    current_user = get_jwt_identity()
    
    req_data = request.get_json()
    

    filters = {
        "item_id": int(req_data["id"])
    }

    desarrollo = find_item_by_filter_from_app(
        podio_client_desarrollos(),
        int(os.getenv("PODIO_INZIGNA_DESARROLLOS_APP_ID")),
        filters,
    )
   
    url_image = ""
    logo = ""
    portada = ""
    posicion = ""
    amenidades = ""
    cotizacion = ""


    if req_data["logo"]==[]:
        logo = desarrollo[0]["imagen-logo"]
    else:
        logo = req_data["logo"]

    if req_data["portada"]==[]:
        portada = desarrollo[0]["imagen-portada"]
    else:
        portada = req_data["portada"]

    if req_data["posicionFile"]==[]:
        posicion = desarrollo[0]["imagen-mapa"]
    else:
        posicion = req_data["posicionFile"]

    if req_data["amenidadesFile"]==[]:
        amenidades = desarrollo[0]["imagen-amenidades"]
    else:
        amenidades = req_data["amenidadesFile"]

    if req_data["cotizacionFile"]==[]:
        cotizacion = desarrollo[0]["imagen-cotizacion"]
    else:
        cotizacion = req_data["cotizacionFile"]

    if req_data["imagenes"]!=[None]:
        for cont in range(0,len(req_data["imagenes"])):
            if(cont == (len(req_data["imagenes"]))-1):
                url_image = url_image + req_data["imagenes"][cont]
            else:
                url_image = url_image + req_data["imagenes"][cont] + ","
    else:
        url_image = desarrollo[0]["imagenes-3"]

    desarrollo = update_item(
        podio_client_desarrollos(),
        int(req_data["id"]),
        {
            "imagen-logo": logo,
            "nombre": req_data["nombre"],
            "ciudad": req_data["ciudad"],
            "direccion": req_data["direccion"],
            "link": req_data["link"],
            "imagen-portada": portada,
            "imagen-mapa": posicion,
            "imagen-amenidades": amenidades,
            "imagen-cotizacion": cotizacion,
            "imagenes-3": url_image,
        }
    )

    return { "msg": "Actualizado Correctamente"}

@desarrollos.route('/delete-development', methods=['POST'])
@jwt_required
def delete_development():
    ###### Get current user ################
    current_user = get_jwt_identity()

    req_data = request.get_json()

    desarrollo = delete_item(
        podio_client_desarrollos(),
        int(req_data["id"])
    )
    
    return { "msg": "Proceso Correcto"}

@desarrollos.route('/get-development-by-id', methods=['POST'])
@jwt_required
def get_development_by_id():
    ###### Get current user ################
    current_user = get_jwt_identity()
    req_data = request.get_json()
    
    filters = {
        "item_id": int(req_data["id"])
    }

            
    desarrollo = find_item_by_filter_from_app(
        podio_client_desarrollos(),
        int(os.getenv("PODIO_INZIGNA_DESARROLLOS_APP_ID")),
        filters,
    )

    return jsonify(desarrollo)


@desarrollos.route('/download-image', methods=['POST'])
def descargarImagenes():
    req_data = request.get_json()
    img_portada = downloadimage(req_data["imagen-portada"])
    img_amenidades = downloadimage(req_data["imagen-amenidades"])
    lista_carrusel = []
    for imagen in req_data["imagen-carrusel"]:
        img_carrusel = downloadimage(imagen)
        lista_carrusel.append(img_carrusel)
    img_mapa = downloadimage(req_data["imagen-mapa"])
    img_posicionamiento = downloadimage(req_data["imagen-posicionamiento"])
    img_plano = downloadimage(req_data["imagen-plano-isometrico"])
    img_logo = downloadimage(req_data["imagen-logo"])
    img_cotizacion = downloadimage(req_data["imagen-cotizacion"])
    
    return {
        "imagen-portada": img_portada,
        "imagen-amenidades": img_amenidades,
        "imagen-carrusel": lista_carrusel,
        "imagen-mapa": img_mapa,
        "imagen-posicionamiento": img_posicionamiento,
        "imagen-plano-isometrico": img_plano,
        "imagen-logo": img_logo,
        "imagen-cotizacion": img_cotizacion
    }


def downloadimage(url):
    ###### Get current user ################
    req = requests.get(url)
    response = req.content
    name,tipo = os.path.splitext(url)
    with open(("image"+tipo), "wb") as f:
        f.write(response)
    
    data = base64.b64encode(open("image"+tipo, 'rb').read()).decode()
    if(tipo==".png"):
        code_image = "data:image/png;base64,"+data
    elif(tipo==".jpg"):
        code_image = "data:image/jpg;base64,"+data
    else:
        code_image = "data:image/jpeg;base64,"+data
    os.remove("image"+tipo)
    return str(code_image)

@desarrollos.route('/convert-image-pdf', methods=['POST'])
def convertirImageToPDF():
    
    req_data = request.get_json()
    
    working_dict = os.getcwd()
    data = req_data["archivo"].replace("data:image/png;base64,", "")
    img_data = base64.b64decode(data)
    
    file_name = "archivo.png"
    with open(file_name, "wb") as f:
        f.write(img_data)
    
    img_path = working_dict + "/" + file_name
    pdf_path = working_dict+"/archivo.pdf"
    
    image = Image.open(img_path) 
    pdf_bytes = img2pdf.convert(image.filename)
    file = open(pdf_path, "wb")
    file.write(pdf_bytes)
    image.close()
    file.close()
    
    data = base64.b64encode(open(pdf_path, 'rb').read()).decode()
    os.remove("archivo.pdf")
    os.remove("archivo.png")
    return {
        "archivo": "data:application/pdf;base64,"+data
    }