##### IMPORTING MODULES #####
from flask import (
    jsonify,
    request,
    Blueprint
)
from flask_jwt_extended import (
    jwt_required,
    get_jwt_identity
)
import os
import requests
from dotenv import load_dotenv
import json
from user_manager import *
from podio_auth import (
    podio_client_configuracion
)
load_dotenv()
######################################

clientes = Blueprint('clientes', __name__)

@clientes.route('/get-clients', methods=['GET'])
@jwt_required
def get_clients():

    token_podio = find_item_by_filter_from_app(
        podio_client_configuracion(),
        int(os.getenv("PODIO_INZIGNA_CONFIGURACION_APP_ID")),
        { "key": 'Tokens' }
    )

    if(len(token_podio)==0):
        return { "msg": "No se encontro el access token" }

    token = json.loads(token_podio[0]["value"].replace("'",'"'))

    url= 'https://www.zohoapis.com/crm/v2/Contacts'
    

    headers = {                        
        'Authorization': 'Zoho-oauthtoken '+token['access_token'],
        'If-Modified-Since': '2020-05-15T12:00:00+05:30'
    }

    parameters = {
        'page': 1,
        'per_page': 100
    }

    response = requests.get(url=url, headers=headers, params=parameters)
    if 'message' in response.json():
        new_token = refreshToken(token_podio[0]["item_id"],token)
        headers['Authorization'] = 'Zoho-oauthtoken '+new_token
        response = requests.get(url=url, headers=headers, params=parameters)
    
    current_user = get_jwt_identity()
    if(current_user["perfil"]=="Administrador"):
        return response.json()
    else:
        lista_cliente = []
        for cliente in response.json()["data"]:
            if(cliente["Owner"]["email"]==current_user["email"][0]["value"]):
                lista_cliente.append(cliente)
        
        return { "data": lista_cliente }


@clientes.route('/create-client', methods=['POST'])
@jwt_required
def create_clients():
    
    req_data = request.get_json()

    token_podio = find_item_by_filter_from_app(
        podio_client_configuracion(),
        int(os.getenv("PODIO_INZIGNA_CONFIGURACION_APP_ID")),
        { "key": 'Tokens' }
    )

    if(len(token_podio)==0):
        return { "msg": "No se encontro el access token" }

    token = json.loads(token_podio[0]["value"].replace("'",'"'))

    url = 'https://www.zohoapis.com/crm/v2/Contacts'

    headers = {
        'Authorization': 'Zoho-oauthtoken ' +token['access_token'],
    }

    request_body = dict()
    request_data = list()
    current_user = get_jwt_identity()

    user = {
        'Email': req_data["email"],
        'Last_Name': req_data["last_name"],
        'First_Name': req_data["name"],
        'Phone': req_data["phone"],
        'Owner': current_user["id"],
        'Account_Name': req_data["name"]+' '+req_data["last_name"],
        'Lead_Status': 'Contacted',
        "Lead_Source": "Pagina Web"
    }

    request_data.append(user)

    request_body['data'] = request_data

    trigger = [
        'approval',
        'workflow',
        'blueprint'
    ]

    request_body['trigger'] = trigger

    response = requests.post(url=url, headers=headers, data=json.dumps(request_body).encode('utf-8'))
    if "message" in response.json():
        new_token = refreshToken(token_podio[0]["item_id"],token)
        headers['Authorization'] = 'Zoho-oauthtoken '+new_token
        response = requests.post(url=url, headers=headers, data=json.dumps(request_body).encode('utf-8'))

    return {
        "msg": "Proceso Correcto"
    }

@clientes.route('/update-client', methods=['POST'])
@jwt_required
def update_clients():
    req_data = request.get_json()

    token_podio = find_item_by_filter_from_app(
        podio_client_configuracion(),
        int(os.getenv("PODIO_INZIGNA_CONFIGURACION_APP_ID")),
        { "key": 'Tokens' }
    )

    if(len(token_podio)==0):
        return { "msg": "No se encontro el access token" }

    token = json.loads(token_podio[0]["value"].replace("'",'"'))

    url = 'https://www.zohoapis.com/crm/v2/Contacts'

    headers = {
        'Authorization': 'Zoho-oauthtoken ' +token['access_token'],
    }

    request_body = dict()
    request_data = list()
    current_user = get_jwt_identity()
    user = {
        'id': req_data["id"],
        'Email': req_data["email"],
        'Account_Name': req_data["name"]+' '+req_data["last_name"],
        'Owner': current_user["id"],
        'Last_Name': req_data["last_name"],
        'First_Name': req_data["name"],
        'Phone': req_data["phone"],
        'Lead_Status': 'Contacted',
        "Lead_Source": "Pagina Web"
    }

    request_data.append(user)

    request_body['data'] = request_data

    trigger = [
        'approval',
        'workflow',
        'blueprint'
    ]

    request_body['trigger'] = trigger

    response = requests.put(url=url, headers=headers, data=json.dumps(request_body).encode('utf-8'))
    if "message" in response.json():
        new_token = refreshToken(token_podio[0]["item_id"],token)
        headers['Authorization'] = 'Zoho-oauthtoken '+new_token
        response = requests.put(url=url, headers=headers, data=json.dumps(request_body).encode('utf-8'))
    
    return {
        "msg": "Proceso Correcto"
    }

@clientes.route('/get-code',  methods=['GET'])
def getCode():
    code = request.args.get('code')
    
    url = "https://accounts.zoho.com/oauth/v2/token"

    payload={
        'grant_type': 'authorization_code',
        'client_id': '1000.LKA091TVSGMOX95PIQ50HGE1VN7XXR',
        'client_secret': '62e57f2a245acad9a68eed0ca24c2d84b0cf04ce1a',
        'redirect_uri': 'https://inzignacapital.dsindigo.com:8443/get-code',
        'code': str(code)
    }

    files=[]
    headers = {}

    response = requests.request("POST", url, headers=headers, data=payload, files=files)

    token_podio = find_item_by_filter_from_app(
        podio_client_configuracion(),
        int(os.getenv("PODIO_INZIGNA_CONFIGURACION_APP_ID")),
        {
            "key": 'Tokens'
        }
    )

    if(len(token_podio)==0):
        return {
            "msg": "No se encontro el access token"
        }
        create_item(
            podio_client_configuracion(),
            int(os.getenv("PODIO_INZIGNA_CONFIGURACION_APP_ID")),
            {
                str(response.text)
            }
        )

        return {
            "Proceso Correcto"
        }

    
    update_item(
        podio_client_configuracion(),
        token_podio[0]["item_id"],
        {
            "value": str(response.text)
        }
    )

    return {
        "mg": "Proceso correcto"
    }

def refreshToken(item_id,data):

    url = 'https://accounts.zoho.com/oauth/v2/token'
    
    parametros = {
        'refresh_token': data["refresh_token"],
        'client_id': '1000.LKA091TVSGMOX95PIQ50HGE1VN7XXR',
        'client_secret': '62e57f2a245acad9a68eed0ca24c2d84b0cf04ce1a',
        'grant_type': 'refresh_token'
    } 
    
    response = requests.request("POST",url,params=parametros)
    
    string_json = {
        "refresh_token":str(data["refresh_token"]),
        "access_token": json.loads(response.text)["access_token"]
    }

    update_item(
        podio_client_configuracion(),
        item_id,
        { "value": str(string_json) }
    )

    return string_json["access_token"]