##### IMPORTING MODULES #####
from user_manager import *
from podio_auth import (
    podio_client_solicitudes

)
from flask import (
    jsonify,
    request,
    Blueprint
)
from flask_jwt_extended import (
    jwt_required,
    get_jwt_identity
)
import os, ftplib, subprocess
from dotenv import load_dotenv
load_dotenv()
######################################

solicitudes = Blueprint('solicitudes', __name__)

@solicitudes.route('/get-solicitudes', methods=['GET'])
@jwt_required
def get_solicitudes():
    ###### Get current user ################
    current_user = get_jwt_identity()

    filters = {
        "accion": 3
    }
    if(current_user["perfil"]=="Cliente"):
        filters.update(
            {
                "cliente": current_user["item_id"]
            }
        )
    elif(current_user["perfil"]=="Soporte"):
        filters.update(
            {
                "consultor": current_user["item_id"]
            }
        )
            
    solicitudes = find_item_by_filter_from_app(
        podio_client_solicitudes(),
        int(os.getenv("PODIO_SERVICE_DESK_SOLICITUDES_APP_ID")),
        filters,
    )

    return jsonify(solicitudes)


@solicitudes.route('/create-solicitudes', methods=['POST'])
@jwt_required
def create_solicitudes():
    ###### Get current user ################
    current_user = get_jwt_identity()
    
    req_data = request.get_json()
    
    url_image = ""
    
    insertColumn={
            "proyecto": req_data["proyecto"],
            "tipo": int(req_data["tipo"]),
            "descripcion": req_data["descripcion"],
            "cliente": current_user["item_id"],
            "fecha-consulta": {
                "start": today()
            },
            "accion": 3,
            "estatus": 1
        }

    if req_data["image"]!='':
        for cont in range(0,len(req_data["image"])):
            if(cont == (len(req_data["image"]))-1):
                url_image = url_image + req_data["image"][cont]
            else:
                url_image = url_image + req_data["image"][cont] + ","

            insertColumn={
            "proyecto": req_data["proyecto"],
            "tipo": int(req_data["tipo"]),
            "descripcion": req_data["descripcion"],
            "cliente": current_user["item_id"],
            "fecha-consulta": {
                "start": today()
            },
            "url-image": url_image,
            "accion": 3,
            "estatus": 1
        }

    solicitudes = create_item(
        podio_client_solicitudes(),
        int(os.getenv("PODIO_SERVICE_DESK_SOLICITUDES_APP_ID")),
        insertColumn
    )

    return {
        "msg": "podio"
    }

@solicitudes.route('/get-solicitudes-filter', methods=['POST'])
@jwt_required
def get_solicitudes_filter():
    ###### Get current user ################
    current_user = get_jwt_identity()
    req_data = request.get_json()
    
    filters = {
        "accion": 3
    }
    
    if(current_user["perfil"]=="Administrador"):
        filters.update(
            {
                "estatus": req_data["estatus"]
            }
        )
    elif(current_user["perfil"]=="Soporte"):
        filters.update(
            {
                "consultor": current_user["item_id"],
                "estatus": req_data["estatus"]
            }
        )
    else:
        return {
            "msg": "usuario sin privilegios"
        }
            
    solicitudes = find_item_by_filter_from_app(
        podio_client_solicitudes(),
        int(os.getenv("PODIO_SERVICE_DESK_SOLICITUDES_APP_ID")),
        filters,
    )

    return jsonify(solicitudes)

@solicitudes.route('/solicitudes-acciones', methods=['POST'])
@jwt_required
def solicitudes_accion():
    current_user = get_jwt_identity()
    req_data = request.get_json()

    if(req_data["acciones"] == "Archivar"):
        req_data["acciones"]=1
    else:
        req_data["acciones"]=2
    
    update_item(
        podio_client_solicitudes(),
        req_data["item_id"],
        {
            "accion": req_data["acciones"]
        }
    )

    return {
        "msg": "Proceso Correcto"
    }

@solicitudes.route('/solicitudes-item', methods=['POST'])
@jwt_required
def solicitud_item():

    current_user = get_jwt_identity()
    req_data = request.get_json()

    solicitud = find_item_id(
        podio_client_solicitudes(),
        req_data["item_id"]
    )

    return jsonify(solicitud)

@solicitudes.route('/get-solicitudes-archivadas', methods=['GET'])
@jwt_required
def get_solicitudes_archivadas():
    ###### Get current user ################
    current_user = get_jwt_identity()

    filters = {
        "accion": 1
    }
    if(current_user["perfil"]=="Cliente"):
        filters.update(
            {
                "cliente": current_user["item_id"]
            }
        )
    elif(current_user["perfil"]=="Soporte"):
        filters.update(
            {
                "consultor": current_user["item_id"]
            }
        )
            
    solicitudes = find_item_by_filter_from_app(
        podio_client_solicitudes(),
        int(os.getenv("PODIO_SERVICE_DESK_SOLICITUDES_APP_ID")),
        filters,
    )

    return jsonify(solicitudes)

@solicitudes.route('/finalizar-solicitud', methods=['POST'])
@jwt_required
def finalizar_solicitud():
    current_user = get_jwt_identity()
    req_data = request.get_json()

    update_item(
        podio_client_solicitudes(),
        int(req_data["id"]),
        {
            "estatus": 2,
            "calificacion":int(req_data['calificacion'])
        }) 
    return {
        "msg": "Proceso Correcto"
    }                

@solicitudes.route('/actualizar-solicitudes', methods=['POST'])
@jwt_required
def actualizar_solicitudes():
    ###### Get current user ################
    current_user = get_jwt_identity()
    
    req_data = request.get_json()
    
    solicitud = update_item(
        podio_client_solicitudes(),
        req_data["id"],
        {
            "proyecto":req_data["proyecto"],
            "consultor":req_data["consultor"]
        }
    )

    return {
        "msg": "Se ha actualizado el proyecto y el consultor en la solicitud"
    }
