##### IMPORTING MODULES #####
from user_manager import *
from podio_auth import (
    podio_client_conversaciones,
    podio_client_solicitudes
)
from flask import (
    jsonify,
    request,
    Blueprint
)
from flask_jwt_extended import (
    jwt_required,
    get_jwt_identity
)
import os, ftplib, subprocess
from dotenv import load_dotenv
load_dotenv()
######################################

conversacion = Blueprint('conversacion', __name__)

@conversacion.route('/get-conversacion', methods=['POST'])
@jwt_required
def get_conversacion():
    ###### Get current user ################
    current_user = get_jwt_identity()
    req_data = request.get_json()

    filters = {
        "solicitud": req_data["item_id"]
    }
    conversaciones = find_item_by_filter_from_app(
        podio_client_conversaciones(),
        int(os.getenv("PODIO_SERVICE_DESK_CONVERSACIONES_APP_ID")),
        filters
    )

    if(len(conversaciones)>0):
        return jsonify(orderByDate(conversaciones))
    else:
        return jsonify([])
@conversacion.route('/create-conversacion', methods=['POST'])
@jwt_required
def create_conversacion():
    ###### Get current user ################
    current_user = get_jwt_identity()

    req_data = request.form
    req_file = request.files
    if "file" in req_file.keys():
        data_file = upload_file(
        "66.70.179.143",
        "archivosindigo",
        "MPz2DaX7x3LR",
        req_file["file"],
        "service_desk"
        ) 
    else:
        data_file = {
            "url":"Sin información"
        }
       
    create_item(
        podio_client_conversaciones(),
        int(os.getenv("PODIO_SERVICE_DESK_CONVERSACIONES_APP_ID")),
        {
            "mensaje": req_data["mensaje"],
            "url-image": data_file["url"],
            "solicitud": int(req_data["item_id"]),
            "user": current_user["item_id"],
            "fecha": today()
        }
    )

    solicitud = update_item(
        podio_client_solicitudes(),
        int(req_data["item_id"]),
        {
            "ultima-fecha-de-respuesta" : today()
        }
    )

    return {
        "msg": "Proceso Exitoso"
    }