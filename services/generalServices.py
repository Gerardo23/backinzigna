##### IMPORTING MODULES #####
from podio_auth import (
    podio_client_users,
    podio_client_configuracion
)
from user_manager import *
from flask import (
    request,
    jsonify,
    Blueprint
)
from flask_jwt_extended import (
    create_access_token,
    jwt_required,
    get_jwt_identity
)
from flask_bcrypt import (
    generate_password_hash,
    check_password_hash
)
from services.clientes import (
    refreshToken
)
import os, string, random
import json
import requests
from dotenv import load_dotenv
load_dotenv()
######################################

general = Blueprint('general', __name__)


@general.route('/login', methods=['POST'])
def login():

    req_data = request.get_json()

    resp = find_item_by_filter(
        podio_client_users(),
        int(os.getenv("PODIO_INZIGNA_USERS_APP_ID")),
        {
            "email": [req_data["email"]]
        }
    )

    msg = "Proceso Correcto"
    if len(resp["items"]) > 0:
        user_data = get_all_item_fields(resp["items"][0]["fields"],resp["items"][0]["item_id"])
        if(not "id" in user_data):
            idCRM = ""
            listaCRM = getUserCRM()
            for n in listaCRM["users"]:
                if(user_data["perfil"]=="Administrador"):
                    if(n["email"]=="jzaga@inzigna.com"):
                        idCRM=n["id"]
                else:
                    if(n["email"]==req_data["email"]):
                        idCRM=n["id"]
            
            if(idCRM==""):
                return {"msg":"Este usuario no existe en CRM"},500
            
            update_item(
                podio_client_users(),
                user_data["item_id"],
                {
                    "id": idCRM
                }
            )
            user_data.update({"id":idCRM})
        

        if user_data["password"] == req_data["password"]:
            
            access_token = create_access_token(
                identity=user_data,
                expires_delta=False
            )
            psw_hasher(req_data["password"],resp["items"][0]["item_id"], access_token)
            
            response = {
                "msg": msg,
                "access_token": access_token,
                "perfil": user_data["perfil"],
                "nombre": user_data["nombre"],
                "item_id": user_data["item_id"],

            }

            return jsonify(response)
        
        elif check_password_hash(user_data["password"], req_data["password"]):
            
            access_token = create_access_token(
                identity=user_data,
                expires_delta=False
            )
            
            response = {
                "msg": msg,
                "access_token": access_token,
                "perfil": user_data["perfil"],
                "nombre": user_data["nombre"],
                "item_id": user_data["item_id"],

            }

            return jsonify(response)
        else:
            msg = "Contraseña Incorrecta"
    else:
        msg="El Usuario No Existe"
    response = {
        "msg": msg
    }
    print(msg)
    return jsonify(response)


@general.route('/password-recover', methods=['POST'])
def recover_psw():

    req_data = request.get_json()

    resp = find_item_by_filter(
        podio_client_users(),
        int(os.getenv("PODIO_INZIGNA_USERS_APP_ID")),
        {
            "email": [req_data['email']]
        },
    )
    
    if(len(resp["items"])>0):
        new_password = ''.join(random.choices(
            string.ascii_uppercase + string.digits, k=8))

        update_item(
            podio_client_users(),
            resp['items'][0]['item_id'],
            {
                'password': new_password,
                'password2': new_password,
            }
        )
        update_item(
            podio_client_users(),
            resp['items'][0]['item_id'],
            {
                'acciones': 1
            }
        )

        msg = "Proceso Correcto"

    else:
        msg="El Usuario No Existe"

    return {'msg': msg}


def psw_hasher(password, item_id, access_token):
    
    encoded_pswd = generate_password_hash(password).decode("utf8")

    update_item(
        podio_client_users(),
        item_id,
        {
            "password": encoded_pswd
        }
    )

    return encoded_pswd

@general.route('/create-cliente', methods=['POST'])
def create_cliente():
    
    req_data = request.get_json()

    resp = find_item_by_filter(
        podio_client_users(),
        int(os.getenv("PODIO_INZIGNA_USERS_APP_ID")),
        {
            "email":[req_data['email']]
        },
    )

    if(len(resp["items"])>0):
        return {
            "msg": "El correo ingresado ya existe"
         }
    else:

        create_item(
            podio_client_users(),
            int(os.getenv("PODIO_INZIGNA_USERS_APP_ID")),
            {
                "nombre": req_data["nombre"],
                "email": [
                    {
                        "type": "work",
                        "value": req_data["email"]
                    }
                ],
                "password2": req_data['password'],
                "password": generate_password_hash( req_data['password']).decode("utf8"),
                "perfil": 1,
                "empresa":req_data["empresa"]
            }
        )

        return {
            "msg": "Proceso Correcto"
        }


@general.route('/create-soporte', methods=['POST'])
@jwt_required
def create_soporte():
    current_user = get_jwt_identity()
    if(current_user["perfil"] != "Administrador"):
        return {
            "msg": "Usuario sin privilegios"
        }
    req_data = request.get_json()

    users = find_item_by_filter_from_app(
        podio_client_users(),
        int(os.getenv("PODIO_INZIGNA_USERS_APP_ID")),
        {
        }
    )
    
    isset=False
    for row in users:
        if row["email"]==req_data['email']:
            isset=True
            break

    if isset==False:                        
        create_item(
            podio_client_users(),
            int(os.getenv("PODIO_INZIGNA_USERS_APP_ID")),
            {
                "nombre": req_data["nombre"],
                "email": [
                    {
                        "type": "work",
                        "value": req_data["email"]
                    }
                ],
                "perfil": 2,
                "password":req_data['password'],
                "password2":req_data['password']
            }
        )
        return {
                "msg": "Proceso Correcto"
            }    
    else:
        return {
                "msg": "Usuario existente"
            } 
    

@general.route('/edit-soporte', methods=['POST'])
@jwt_required
def edit_soporte():
    current_user = get_jwt_identity()
    if(current_user["perfil"] != "Administrador"):
        return {
            "msg": "Usuario sin privilegios"
        }
    
    req_data = request.get_json()

    users = find_item_by_filter_from_app(
        podio_client_users(),
        int(os.getenv("PODIO_INZIGNA_USERS_APP_ID")),
        {
        }
    )

    isset=False
    for row in users:
        if row["item_id"]==int(req_data['id']):
            if row["email"]!=req_data['email']:
                for row2 in users:
                    if row2["email"]==req_data['email']:
                        isset=True
                        break
    if isset==False:
           
        update_item(
                podio_client_users(),
                int(req_data['id']),
                {
                    "nombre": req_data["nombre"],
                    "email": [
                        {
                            "type": "work",
                            "value": req_data["email"]
                        }
                    ],
                    "password":req_data["password"],
                    "password2":req_data["password"]
                }
            )
        
        return {
            "msg": "Proceso Correcto"
        }
    else:
        return {
            "msg":"Usuario existente"
        }

@general.route('/delete-soporte', methods=['POST'])
@jwt_required
def delete_soporte():
    current_user = get_jwt_identity()
    if(current_user["perfil"] != "Administrador"):
        return {
            "msg": "Usuario sin privilegios"
        }
    
    req_data = request.get_json()

    duser=delete_item(
        podio_client_users(),
        int(req_data['id'])
    )
    
    return {
        "msg": "Proceso Correcto"
    }

def getUserCRM():

    token_podio = find_item_by_filter_from_app(
        podio_client_configuracion(),
        int(os.getenv("PODIO_INZIGNA_CONFIGURACION_APP_ID")),
        { "key": 'user' }
    )

    if(len(token_podio)==0):
        return { "msg": "No se encontro el access token" }

    token = json.loads(token_podio[0]["value"].replace("'",'"'))

    url = 'https://www.zohoapis.com/crm/v2/users'
    
    headers = {                        
        'Authorization': 'Zoho-oauthtoken '+token['access_token'],
        'If-Modified-Since': '2020-05-15T12:00:00+05:30'
    }

    parameters = {
        'type': 'AllUsers'
    }

    response = requests.get(url=url, headers=headers, params=parameters)
    if "message" in response.json():
        new_token = refreshToken(token_podio[0]["item_id"],token)
        headers['Authorization'] = 'Zoho-oauthtoken '+new_token
        response = requests.get(url=url, headers=headers, params=parameters)
    
    print(response.json())
        
    return  response.json()

