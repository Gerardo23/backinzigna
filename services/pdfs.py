##### IMPORTING MODULES #####
from user_manager import *
from flask import (
    jsonify,
    request,
    Blueprint
)
from dotenv import load_dotenv
load_dotenv()

from xhtml2pdf import pisa
from jinja2 import Template
import os

pdfs = Blueprint('pdfs', __name__)

@pdfs.route('/convert', methods=['POST'])
def convertfile():
        
    base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    outputFilename = "test.pdf"
    resultFile = open(outputFilename, "w+b")
    template = Template(open(os.path.join(base_dir, '.\\services\\muestra.html')).read())
    html  = template.render(request.get_json())
    pisaStatus = pisa.CreatePDF(
        html,
        dest=resultFile
    )
    resultFile.close()

    return {
        "msg":""
    }