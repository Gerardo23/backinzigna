##### IMPORTING MODULES #####
from user_manager import *
from flask import (
    jsonify,
    request,
    Blueprint
)
import os, ftplib
from dotenv import load_dotenv
load_dotenv()
######################################

uploadImage = Blueprint('uploadImage', __name__)

@uploadImage.route('/upload-files', methods=['POST'])
def uploadFiles():

    if 'file' in request.files:
        data_file = upload_file(
            "66.70.179.143",
            "archivosindigo",
            "MPz2DaX7x3LR",
            request.files["file"],
            "inzigna"
        )
        return {
        "url": data_file["url"]
        }
    else:
        return jsonify({"message": "No file found"})
    
   