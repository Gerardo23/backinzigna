##### IMPORTING MODULES #####
from user_manager import *
from podio_auth import (
    podio_client_cotizaciones,
    podio_user_simon,
    podio_client_historial
)
from flask import (
    jsonify,
    request,
    Blueprint
)
from flask_jwt_extended import (
    jwt_required,
    get_jwt_identity
)
import os, ftplib, subprocess
from dotenv import load_dotenv
load_dotenv()
######################################

cotizaciones = Blueprint('cotizaciones', __name__)

@cotizaciones.route('/get-quotations', methods=['GET'])
@jwt_required
def get_quotations():
    ###### Get current user ################
    current_user = get_jwt_identity()

    usuarios = find_item_by_filter_from_app(
        podio_client_cotizaciones(),
        int(os.getenv("PODIO_INZIGNA_COTIZACIONES_APP_ID")),
        {
        }
    )

    return jsonify(usuarios)

@cotizaciones.route('/get-quotation', methods=['POST'])
@jwt_required
def get_quotation():
    ###### Get current user ################
    current_user = get_jwt_identity()
    req_data = request.get_json()
    response = podio_client_cotizaciones().Item.find(req_data["item_id"])

    return jsonify(get_all_item_fields(response["fields"]))

@cotizaciones.route('/get-quotations-for-seller', methods=['POST'])
@jwt_required
def get_quotations_for_seller():
    ###### Get current user ################
    current_user = get_jwt_identity()

    req_data = request.get_json()

    usuarios = find_item_by_filter_from_app(
        podio_client_cotizaciones(),
        int(os.getenv("PODIO_INZIGNA_COTIZACIONES_APP_ID")),
        {
            "vendedor": int(req_data["id"])
        }
    )

    return jsonify(usuarios)

@cotizaciones.route('/get-quotation-by-id', methods=['POST'])
@jwt_required
def get_quotation_by_id():
    ###### Get current user ################
    current_user = get_jwt_identity()
    req_data = request.get_json()
    
    filters = {
        "item_id": int(req_data["id"])
    }

            
    cotizacion = find_item_by_filter_from_app(
        podio_client_cotizaciones(),
        int(os.getenv("PODIO_INZIGNA_COTIZACIONES_APP_ID")),
        filters,
    )

    return jsonify(cotizacion)

@cotizaciones.route('/create-quotation', methods=['POST'])
@jwt_required
def create_quotation():
    ###### Get current user ################

    # cliente: "jonathan.perez@dsindigo.com,Jonathan Pérez Medina" +++
    # departamento: "1925383704"+++
    # desarrollo: "1891124095"+++
    # enganche: "62741.70"+++
    # estatus: "Aprobada"+++
    # fecha: {year: 2021, month: 11, day: 18}+++
    # forma-de-pago: "6"+++
    # mensaje: "holagg"+++
    # moneda: "USD"+++
    # pago: "146397.30"+++
    # plazos: "24399.55"+++
    # precio-total: "214139"++
    # tipo: "1"++
    # vendedor: "1884137986"+++
    # views: "0"+++

    current_user = get_jwt_identity()

    req_data = request.get_json()

    if(req_data["estatus"] == 'Aprobada'): 
        estatus = 1
    if(req_data["estatus"] == 'Cancelada'): 
        estatus = 2
    if(req_data["estatus"] == 'Enviada'): 
        estatus = 3
    if(req_data["estatus"] == 'Rechazada'): 
        estatus = 4
    if(req_data["estatus"] == 'Revisión'): 
        estatus = 5

    if(req_data["mensaje"] == ''):
        mensaje = 'Sin mensaje'
    else:
        mensaje = req_data["mensaje"]

    correoCliente = req_data["cliente"].split(sep=",")[0]
    nombreCliente = req_data["cliente"].split(sep=",")[1]

    cotizacion = create_item(
        podio_client_cotizaciones(),
        int(os.getenv("PODIO_INZIGNA_COTIZACIONES_APP_ID")),
        {
            "vendedor": int(req_data["vendedor"]),
            "cliente": nombreCliente,
            "correo-cliente": correoCliente,
            "tipo-2": req_data["tipo"],
            "estatus": estatus,
            "views": req_data["views"],
            "desarrollo": int(req_data["desarrollo"]),
            "departamento": int(req_data["departamento"]),
            "precio-de-lista": req_data["precio-total"],
            "fecha": str(req_data["fecha"]["year"]) +"-"+ str(req_data["fecha"]["month"]) +"-"+ str(req_data["fecha"]["day"]) ,
            "reservacion": str(5000),
            "enganche": str(req_data["enganche"]),
            "pago": str(req_data["pago"]),
            "mensaje": mensaje,
            "precio-final": str(req_data["precio-total"]),
            "forma-de-pago": req_data["forma-de-pago"],
            "pago-por-plazo": str(req_data["plazos"]),
            "moneda": req_data["moneda"],
        },
    )



    # current_user = get_jwt_identity()
    
    # req_data = request.get_json()
    
    # if(req_data["estatus"] == 'Aprobada'): 
    #     estatus = 1
    # if(req_data["estatus"] == 'Cancelada'): 
    #     estatus = 2
    # if(req_data["estatus"] == 'Enviada'): 
    #     estatus = 3
    # if(req_data["estatus"] == 'Rechazada'): 
    #     estatus = 4
    # if(req_data["estatus"] == 'Revisión'): 
    #     estatus = 5
    # if(req_data["mensaje"] == ''):
    #     mensaje = 'Sin mensaje'
    # else:
    #     mensaje = req_data["mensaje"]

    # correoCliente = req_data["cliente"].split(sep=",")[0]
    # nombreCliente = req_data["cliente"].split(sep=",")[1]

    # create_item(
    #     podio_client_cotizaciones(),
    #     int(os.getenv("PODIO_INZIGNA_COTIZACIONES_APP_ID")),
    #     {
    #         "vendedor": int(req_data["vendedor"]),
    #         "cliente": nombreCliente,
    #         "correo-cliente": correoCliente,
    #         "tipo-2": req_data["tipo"],
    #         "estatus": estatus,
    #         "views": req_data["views"],
    #         "desarrollo": int(req_data["desarrollo"]),
    #         "departamento": int(req_data["departamento"]),
    #         "precio-de-lista": req_data["precio-de-lista"],
    #         "fecha": str(req_data["fecha"]["year"]) +"-"+ str(req_data["fecha"]["month"]) +"-"+ str(req_data["fecha"]["day"]) ,
    #         "descuento": req_data["descuento"],
    #         "forma-de-pago": req_data["forma-de-pago"],
    #         "reservacion": str(req_data["reservacion"]),
    #         "enganche": str(req_data["enganche"]),
    #         "entrega": str(req_data["entrega"]),
    #         "mensaje": mensaje,
    #         "porcentaje-reservacion": str(req_data["porcentajeReservacion"]),
    #         "porcentaje-enganche": str(req_data["porcentajeEnganche"]),
    #         "porcentaje-entrega": str(req_data["porcentajeEntrega"]),
    #         "porcentaje-descuento": str(req_data["porcentajeDescuento"]),
    #         "precio-final": str(req_data["precioFinal"]),
    #     },
    # )
    return {
        "msg": "Agregado correctamente",
        "cotizacion":get_all_item_fields(cotizacion['fields'], cotizacion['item_id'])
    }

@cotizaciones.route('/update-quotation', methods=['POST'])
@jwt_required
def update_quotation():
    ###### Get current user ################
    current_user = get_jwt_identity()

    req_data = request.get_json()

    if(req_data["estatus"] == 'Aprobada'): 
        estatus = 1
    if(req_data["estatus"] == 'Cancelada'): 
        estatus = 2
    if(req_data["estatus"] == 'Enviada'): 
        estatus = 3
    if(req_data["estatus"] == 'Rechazada'): 
        estatus = 4
    if(req_data["estatus"] == 'Revisión'): 
        estatus = 5

    if(req_data["mensaje"] == ''):
        mensaje = 'Sin mensaje'
    else:
        mensaje = req_data["mensaje"]

    correoCliente = req_data["cliente"].split(sep=",")[0]
    nombreCliente = req_data["cliente"].split(sep=",")[1]

    desarrollo = update_item(
        podio_client_cotizaciones(),
        int(req_data["id"]),
{
            "vendedor": int(req_data["vendedor"]),
            "cliente": nombreCliente,
            "correo-cliente": correoCliente,
            "tipo-2": req_data["tipo"],
            "estatus": estatus,
            "desarrollo": int(req_data["desarrollo"]),
            "departamento": int(req_data["departamento"]),
            "precio-de-lista": req_data["precio-total"],
            "fecha": str(req_data["fecha"]["year"]) +"-"+ str(req_data["fecha"]["month"]) +"-"+ str(req_data["fecha"]["day"]) ,
            "reservacion": str(5000),
            "enganche": str(req_data["enganche"]),
            "pago": str(req_data["pago"]),
            "mensaje": mensaje,
            "precio-final": str(req_data["precio-total"]),
            "forma-de-pago": req_data["forma-de-pago"],
            "pago-por-plazo": str(req_data["plazos"]),
            "moneda": req_data["moneda"],
        },
    )



    # current_user = get_jwt_identity()
    
    # req_data = request.get_json()
    
    # if(req_data["estatus"] == 'Aprobada'): 
    #     estatus = 1
    # if(req_data["estatus"] == 'Cancelada'): 
    #     estatus = 2
    # if(req_data["estatus"] == 'Enviada'): 
    #     estatus = 3
    # if(req_data["estatus"] == 'Rechazada'): 
    #     estatus = 4
    # if(req_data["estatus"] == 'Revisión'): 
    #     estatus = 5

    # if(req_data["mensaje"] == ''):
    #     mensaje = 'Sin mensaje'
    # else:
    #     mensaje = req_data["mensaje"]

    # correoCliente = req_data["cliente"].split(sep=",")[0]
    # nombreCliente = req_data["cliente"].split(sep=",")[1]

    # desarrollo = update_item(
    #     podio_client_cotizaciones(),
    #     int(req_data["id"]),
    #     {
    #         "vendedor": int(req_data["vendedor"]),
    #         "cliente": nombreCliente,
    #         "correo-cliente": correoCliente,
    #         "tipo-2": req_data["tipo"],
    #         "estatus": estatus,
    #         "views": req_data["views"],
    #         "desarrollo": int(req_data["desarrollo"]),
    #         "departamento": int(req_data["departamento"]),
    #         "precio-de-lista": req_data["precio-de-lista"],
    #         "fecha": str(req_data["fecha"]["year"]) +"-"+ str(req_data["fecha"]["month"]) +"-"+ str(req_data["fecha"]["day"]) ,
    #         "descuento": req_data["descuento"],
    #         "forma-de-pago": req_data["forma-de-pago"],
    #         "reservacion": str(req_data["reservacion"]),
    #         "enganche": str(req_data["enganche"]),
    #         "entrega": str(req_data["entrega"]),
    #         "mensaje": mensaje,
    #         "porcentaje-reservacion": str(req_data["porcentajeReservacion"]),
    #         "porcentaje-enganche": str(req_data["porcentajeEnganche"]),
    #         "porcentaje-entrega": str(req_data["porcentajeEntrega"]),
    #         "porcentaje-descuento": str(req_data["porcentajeDescuento"]),
    #         "precio-final": str(req_data["precioFinal"]),
    #     },
    # )

    return {
        "msg": "Actualizado correctamente",
    }

@cotizaciones.route('/delete-quotation', methods=['POST'])
@jwt_required
def delete_quotation():
     ###### Get current user ################
    current_user = get_jwt_identity()

    req_data = request.get_json()

    cotizacion = delete_item(
        podio_client_cotizaciones(),
        int(req_data["id"])
    )

    return { "msg": "Proceso Correcto"}

@cotizaciones.route('/send-email', methods=['POST'])
@jwt_required
def send_email():
     ###### Get current user ################
    current_user = get_jwt_identity()

    req_data = request.get_json()
    if req_data["tipo"] == 'Estatico':
        tipoCorreo = 1
    else:
        tipoCorreo = 2

    correo = update_item(
        podio_client_cotizaciones(),
        int(req_data["id"]),
        {
            "acciones": tipoCorreo,
        }
    )

    return { "msg": "Correo enviado","info":correo}

@cotizaciones.route('/get-element-historial', methods=['POST'])
@jwt_required
def getHistorial():
     ###### Get current user ################
    current_user = get_jwt_identity()

    req_data = request.get_json()

    response = podio_client_historial().Item.find(
        int(req_data["id"])
    )

    return jsonify(get_all_item_fields(response["fields"]))

@cotizaciones.route('/actualizar-vista', methods=['POST'])
def actualizarVista():
    
    req_data = request.get_json()
    response = podio_client_cotizaciones().Item.find(
        int(req_data["item_id"])
    )
    
    cotizacion = get_all_item_fields(
        response["fields"],
        response["item_id"]
    )
    
    vista = 0
    if("views" in cotizacion):
        vista = int(cotizacion["views"])
    
    update_item(
        podio_client_cotizaciones(),
        req_data["item_id"],
        {
            "views": str(vista + 1)
        }
    )
    return jsonify(cotizacion)