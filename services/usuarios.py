##### IMPORTING MODULES #####
from user_manager import *
from podio_auth import (
    podio_client_users
)
from flask import (
    jsonify,
    request,
    Blueprint
)
from flask_jwt_extended import (
    jwt_required,
    get_jwt_identity
)
import os, ftplib, subprocess
from dotenv import load_dotenv
load_dotenv()
######################################

usuarios = Blueprint('usuarios', __name__)

@usuarios.route('/get-users', methods=['GET'])
@jwt_required
def get_users():
    ###### Get current user ################
    current_user = get_jwt_identity()

    usuarios = find_item_by_filter_from_app(
        podio_client_users(),
        int(os.getenv("PODIO_INZIGNA_USERS_APP_ID")),
        {
        }
    )

    return jsonify(usuarios)

@usuarios.route('/create-user', methods=['POST'])
@jwt_required
def create_user():
    ###### Get current user ################
    current_user = get_jwt_identity()
    
    req_data = request.form
    req_file = request.files

    resp = find_item_by_filter(
        podio_client_users(),
        int(os.getenv("PODIO_INZIGNA_USERS_APP_ID")),
        {
            "email": [req_data["email"]]
        }
    )

    if(len(resp["items"])>0):
        return {
            "msg": "El correo ingresado ya existe"
        }
    else:
        if "foto" in req_file.keys():
            data_file = upload_file(
            "66.70.179.143",
            "archivosindigo",
            "MPz2DaX7x3LR",
            req_file["foto"],
            "inzigna"
            ) 
        else:
            data_file = {
                "url":"Sin información"
            }

        create_item(
            podio_client_users(),
            int(os.getenv("PODIO_INZIGNA_USERS_APP_ID")),
            {
                "nombre": req_data["nombre"],
                "email": [{"type": "work","value": req_data["email"]}],
                "password": req_data["password"],
                "password2": req_data["password"],
                "perfil": 1,
                "estatus": 1,
                "urlfoto": data_file["url"],
                "firma": req_data["firma"],
            },
        )

        return {
            "msg": "Agregado correctamente",
        }

@usuarios.route('/update-user', methods=['POST'])
@jwt_required
def update_user():
    ###### Get current user ################
    current_user = get_jwt_identity()    

    req_data = request.form
    req_file = request.files

    if "foto" in req_file.keys():
        data_file = upload_file(
        "66.70.179.143",
        "archivosindigo",
        "MPz2DaX7x3LR",
        req_file["foto"],
        "inzigna"
        ) 
    else:
        data_file = {
            "url":"Sin información"
        }

    usuario = update_item(
        podio_client_users(),
        int(req_data["id"]),
        {
            "nombre": req_data["nombre"],
            "password": req_data["password"],
            "password2": req_data["password"],
            "perfil": 1,
            "estatus": 1,
            "urlfoto": data_file["url"],
            "firma": req_data["firma"],
        }
    )

    return { "msg": "Actualizado Correctamente"}

@usuarios.route('/delete-user', methods=['POST'])
@jwt_required
def delete_user():
    ###### Get current user ################
    current_user = get_jwt_identity()

    req_data = request.get_json()

    usuario = delete_item(
        podio_client_users(),
        int(req_data["id"])
    )
    
    return { "msg": "Proceso Correcto"}